#include <iostream>

#include "functions.h"


int main()
{
    using namespace strings;

    {

    using std::cout;

    auto fibs = fibo::to_n(10);

    for (auto i: fibs) {
        cout << i << ", ";
    }
    cout << "\n";

    }

    {

    auto fibs = fibo::to_max(10000);

    for (auto i: fibs) {
        std::cout << i << ", ";
    }
    std::cout << "\n";

    }

    std::cout << is_palindrome("arara") << "\n";
    std::cout << is_palindrome("macaco") << "\n";

    auto v = std::vector<unsigned int>{3, 4, 1, 5, 4242, 2048};

    std::cout << "before vectors::zero_all():\n";
    for (auto i: v) {
        std::cout << i << ", ";
    }
    std::cout << "\n";

    vectors::zero_all(v);

    std::cout << "after vectors::zero_all():\n";
    for (auto i: v) {
        std::cout << i << ", ";
    }
    std::cout << "\n";

    return 0;
}
